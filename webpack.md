# WEBPACK / NPM

## NPM
Permet principalement l'installation et mise à jour des dépendances front-end.

> ```npm install <nom de la dépendance> --save```  # installation d'une bibliothèque et inscription dans le fichier package.json
>
> ```npm update``` # mise à jour des dépendances

# WEBPACK
S'occupe d'orchestrer la gestion des assets (javascript et css principalement). Il peut s'agir de fichiers créés par l'utilisateurs (placés à priori dans `assets/`) ou de bibliothèques récupérées via `npm`

npm s'occupe de lancer webpack :

> ```npm run dev``` # lance webpack pour un environnement de développement
>
> ```npm run watch ``` # surveille le dossier assets/ et relance webpack en cas de modification

Les fichiers générés par webpack se retrouvent par défaut dans `public/build`. Ils sont donc réutilisables depuis les templates twig via `{{ asset('build/<nom du fichier>') }}`
