# SYMFONY 4

## Composer

Symfony dépend de pas mal de briques logicielles. Celles-ci sont récupérées via composer et téléchargées dans le dossier `vendor`

> `composer require  <nom de la dépendance>` #installe la dépendance et met à jour le fichier composer.json
>
> `composer install` # installe les dépendances présentes dans composer.json aux versions définies dans composer.lock
>
> `composer update` # met à jour les dépendances (ce qui a pour effet de mettre à jour le composer.lock)

## Les répertoires de base

-   `src/Controller/`

    >  Définition des points d'entrées et de sortie de l'application via des routes.
    > Retourne des vues calculées à partir de templates auxquelles on passe des objets/variables
    >
    >  `php bin/console debug:router` pour lister les différentes routes
    >  `php bin/console make:controller --help` pour générer un nouveau controller`

-   `src/Entity/`

    > Les entités définissant le schéma de données, relations entre les entités, propriétés des entités, etc. Gros lien avec l'ORM doctrine qui s'occupe des interation avec la base de données. (<https://www.doctrine-project.org/projects/doctrine-orm/en/2.6/index.html>)
    >
    > `php bin/console make:entity` # générer les entités avec les getters/setters
    >
    > `php bin/console doctrine:schema:validate` # vérifier la validité du schéma et la synchro avec la base
    >
    > `php bin/console doctrine:schema:update --force` # mettre à jour la base de données conformément au schéma
    >
    > `php bin/console doctrine:schema:drop --force` # supprime la base de données

-   `src/Form`

    > Contient les formulaires

-   `src/Repository/`

    > Contient les méthodes pour faire des requêtes un peu plus poussées que celle de base (findBy<Property>(), findAll), etc.)

    `src/Command/`
    > Contient les commandes à lancer depuis le container symfony (apache/nginx souvent) ou depuis un makefile par exemple
    > `php bin/console make:command`

-   `templates/`

    > <https://twig.symfony.com/>
    >
    > Contient les templates twig (qui seront appelé par les controller).
    >
    > `php bin/console debug:twig` # liste les fonctions / filtres / etc.
    >
    > `php bin/console lint:twig <nom du template>` # lint un fichier twig

-   `assets/`
    > Contient les fichiers javascript et css. Il seront traités par webpack et les sorties seront dispo dans `public/build`
