# DOCKER-COMPOSE

> <https://docs.docker.com/compose/>

-   `docker-compose.yml` à la racine.
    -   définition des différents containers
    -   mapping avec des volumes locaux (fichiers ou répertoires)
    -   mapping avec des ports locaux (le port local utilisé doit être unique pour tous les containers, globalement, pas seulement au niveau de cette application)

> `docker-compose up -d` # démarre les containers définis dans docker-compose.yml
>
> `docker-compose up –build -d` # build et démarre les containers définis dans docker-compose.yml
>
> `docker compose exec <nomcontainer> <command>` # lance une commande dans un container
